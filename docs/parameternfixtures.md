# PyTest : Parameter and Fixtures

## Parametrized Tests:
- 매개변수화된 테스트를 사용하면 개발자가 다양한 값에 대해 동일한 테스트를 반복해서 실행할 수 있다.
- 매개변수화된 테스트는 데이터 기반 테스트를 수행하고자 할 때 사용한다. 예를 들어, 1000개의 서로 다른 사용자 이름과 비밀번호 조건에 대한 여러 입력 값을 사용하여 로그인 페이지 또는 프로세스를 테스트하려고 한다.
- 이렇게 하면 테스트 코드를 더 추가하지 않고도 커버리지를 늘릴 수 있다.
- 예: `@pytest.mark.parametrize("test_input,expected", [("5+5", 10), ("5-5", 0), ("7*8", 56)])`

다음은 pytest 매개변수화 기능의 사용법을 보여주는 몇몇 예이다.

```python
import pytest

def add(a, b):
    return a + b

@pytest.mark.parametrize("a, b, expected", [
    (2, 3, 5),
    (5, 7, 12),
    (-2, 2, 0),
])
def test_addition(a, b, expected):
    assert add(a, b) == expected

@pytest.mark.parametrize("a, b", [
    (2, 3),
    (5, 7),
    (-2, 2),
])
def test_positive_numbers(a, b):
    assert a > 0
    assert b > 0
```

첫 번째 예에서 `test_addition` 테스트 함수는 `pytest.mark.parametrize`를 사용하여 매개변수화되었다. 이 함수를 사용하면 매개변수와 예상 값을 지정하는 튜플 리스트를 제공하여 다른 입력 값으로 동일한 테스트를 실행할 수 있다. 이 경우 테스트는 세 번 실행되며, 매번 `a`, `b`와 `expected`가 다른 값으로 실행된다.

두 번째 예인 `test_positive_numbers`는 예상값이 없는 매개변수화를 보인다. 이 예는 테스트를 여러 번 실행하지만 `a`와 `b`가 모두 양수인지 확인한다. 이렇게 하면 다른 입력 시나리오에 테스트 로직을 재사용할 수 있다.

`pytest`를 실행하면 테스트 결과에 각 매개변수 조합에 대한 개별 결과가 출력된다.

```
====================================================== test session starts =======================================================
platform darwin -- Python 3.11.4, pytest-7.3.0, pluggy-1.0.0
rootdir: /Users/yoonjoonlee/MyProjects/PyCodingEx/PyTestExamples
collected 6 items                                                                                                                

ex_01.py .....F                                                                                                            [100%]

============================================================ FAILURES ============================================================
__________________________________________________ test_positive_numbers[-2-2] ___________________________________________________

a = -2, b = 2

    @pytest.mark.parametrize("a, b", [
        (2, 3),
        (5, 7),
        (-2, 2),
    ])
    def test_positive_numbers(a, b):
>       assert a > 0
E       assert -2 > 0

ex_01.py:20: AssertionError
==================================================== short test summary info =====================================================
FAILED ex_01.py::test_positive_numbers[-2-2] - assert -2 > 0
================================================== 1 failed, 5 passed in 0.04s ===================================================
```

이 경우 `test_positive_numbers`의 매개변수 조합 중 하나가 assertion에 실패했으며 실패는 별도로 보고된다.

## Fixtures:

- fixtures는 테스트를 위해 데이터를 준비하는 데 사용된다. fixtures는 실제 테스트 함수 전(때로는 테스트 함수 후)에 pytest에서 실행하는 함수이다.
- fixtures를 사용하여 테스트에 사용할 데이터 세트를 가져올 수 있다. 예를 들어 DB 연결을 설정하거나 웹 드라이버를 초기화하고 테스트하려는 경우 브라우저를 초기화할 수 있다.
- 해당 파일에 있는 테스트에서만 fixtures를 사용하려면 개별 테스트 파일에 fixtures를 넣을 수 있다.
- 올바른 사용 방법은 여러 테스트 파일에서 사용할 수 있도록 `conftest.py`에 fixtures를 넣는 것이다.
- fixtures 함수는 어떤 이름일 수 있다.
- 테스트 함수 인수를 사용하여 fixtures를 호출한다. 예: `def test_someTest(fixture_name)`
- `pytest.fixture()`를 사용하여 fixtures 표시한다.
- 필수는 아니지만 반환 문을 사용하여 fixtures에서 무언가를 반환할 수 있다.
- fixtures를 호출하는 또 다른 방법도 있다.<br>
`@pytest.mark.usefixtures("fixture_name")`
- 이 경우 픽스처에서 반환을 사용할 수 없다. 따라서 fixtures 내에서 일부 코드를 실행할 때만 사용하시요.

다음은 pytest에서 픽스처의 사용법을 보여주는 예이다.

```python
import pytest

@pytest.fixture
def setup_database():
    # Code to set up the database
    print("Setting up the database...")
    yield
    # Code to tear down the database
    print("Tearing down the database...")

@pytest.fixture
def prepare_data():
    # Code to prepare test data
    print("Preparing test data...")
    yield
    # Code to clean up the test data
    print("Cleaning up test data...")

def test_database_operations(setup_database, prepare_data):
    # Test code that requires the database and prepared data
    print("Running test...")
    assert 1 + 1 == 2
```

이 예에는 `setup_database`와 `prepare_data`라는 두 fixture가 있다. fixture는 `@pytest.fixture` 데코레이터를 사용하여 정의한다.

`setup_database` fixture는 각 테스트를 실행하기 전에 데이터베이스를 설정한다. 이 경우, "Setting up the database..."을 출력하고 테스트 함수에 다시 제어권을 넘긴다. 테스트 함수 실행이 완료되면 fixture는 실행을 재개하고 "Tearing down the database..."을 인쇄한다.

`prepare_data` fixture는 테스트 데이터를 준비한다. 테스트 함수에 제어권을 넘기기 전에 "Preparing test data..."을 출력한다. 테스트 함수가 완료되면 실행을 재개하고 "Cleaning up test data..."을 인쇄한다.

`test_database_operations` 함수는 `setup_database`와 `prepare_data` fixture에 모두 의존하는 테스트 함수이다. 이 함수는 데이터베이스와 준비된 테스트 데이터를 사용하여 테스트 로직을 수행한다. 이 예에서는 단순히 1 + 1이 2와 같다고 assert한다.

`pytest`를 실행하면 출력에 fixture 설정, 테스트 실행과 분해가 표시된다.

```
====================================================== test session starts =======================================================
platform darwin -- Python 3.11.4, pytest-7.3.0, pluggy-1.0.0
rootdir: /Users/yoonjoonlee/MyProjects/PyCodingEx/PyTestExamples
collected 1 item                                                                                                                 

ex_02.py .                                                                                                                 [100%]

======================================================= 1 passed in 0.01s ========================================================
```

이 예에서는 `test_database_operations` 테스트 함수가 실행되고 fixture가 올바르게 설정되고 해제된다. fixture는 데이터베이스, 외부 서비스 또는 테스트의 전제 조건과 같은 테스트 종속성을 설정하고 해제할 수 있는 방법을 제공한다.

## fixture를 사용하여 파라미터화하기
- fixure 함수는 매개변수화할 수 있으며, 이 경우 종속 테스트 집합, 즉 이 fixure에 종속되는 테스트를 실행할 때마다 여러 번 호출된다.
- fixure에서 데이터 기반 테스트를 수행할 수 있다.
- fixure 데코레이터에 파라미터를 전달한다.
- 동일한 특수 fixure "resuest"을 사용하여 fixure에서 파라미터를 액세스하고 반환한다.
- ids 키워드 인수를 사용하여 특정 fixure 값에 대한 출력 testID에 사용되는 문자열을 커스터마이즈할 수 있다.
- 여러 개의 fixure를 사용할 수도 있다. 이 경우 테스트 횟수는 픽스처의 각 파라미터를 조합하여 계산된다.

다음은 `pytest` 매개변수화와 함께 fixure를 사용하는 방법을 보여주는 예이다.

```python
import pytest

@pytest.fixture
def multiply_fixture(request):
    factor = request.param
    yield factor

@pytest.mark.parametrize("multiply_fixture", [2, 3, 4], indirect=True)
def test_multiplication(multiply_fixture):
    result = multiply_fixture * 5
    assert result % multiply_fixture == 0
```

이 예에서는 `multiply_fixture`라는 fixture를 활용하는 `test_multiplication`이라는 테스트 함수가 있다. fixture는 `pytest.fixture` 데코레이터를 사용하여 정의되며, `request` 객체를 매개변수로 받는다.

`multiply_fixture` fixture는 `pytest.mark.parametrize`를 사용하여 파라미터화된다. 값 리스트 `[2, 3, 4]`를 제공하며, 이 값은 `multiply_fixture` fixture에 파라미터로 전달된다. `indirect=True` 인수는 테스트 함수에서 fixture를 파라미터로 사용해야 함을 나타낸다.

테스트 함수 내에서 `multiply_fixture` 파라미터를 통해 fixture의 값에 액세스한다. 이 경우 fixture 값에 5를 곱하고 결과를 fixture 값으로 나눌 수 있다고 가정한다.

`pytest`를 실행하면 테스트 결과에 각 파라미터 값에 대한 개별 결과가 출력된다.

```
====================================================== test session starts =======================================================
platform darwin -- Python 3.11.4, pytest-7.3.0, pluggy-1.0.0
rootdir: /Users/yoonjoonlee/MyProjects/PyCodingEx/PyTestExamples
collected 3 items                                                                                                                

ex_03.py ...                                                                                                               [100%]

======================================================= 3 passed in 0.01s ========================================================
```

이 예에서 `test_multiplication` 테스트 함수는 `parametrize` 데코레이터에 제공된 각 파라미터 값에 대해 한 번씩 세 번 실행된다. 실행할 때마다 `multiply_fixture` fixture에 다른 값을 사용하므로 다른 입력으로 테스트 함수의 기능을 테스트할 수 있다.

## fixture 범위

- fixture에는 범위와 수명도 있다.
    - pytest 픽스처의 기본 범위는 함수 범위이다.
    - 함수: 테스트당 한 번 실행되며, 픽스처 범위의 기본값이다. 테스트가 끝나면 fixture가 소멸된다.
    - 클래스: 테스트 클래스당 한 번 실행된다.
    - 모듈: 모듈당 한 번 실행
    - 세션: 세션당 한 번 실행

다음은 `pytest`에서 다양한 범위의 fixture를 보여주는 예이다.

```python
import pytest

@pytest.fixture(scope="function")
def function_fixture():
    print("Setup for function-level fixture")
    yield
    print("Teardown for function-level fixture")

@pytest.fixture(scope="class")
def class_fixture():
    print("Setup for class-level fixture")
    yield
    print("Teardown for class-level fixture")

@pytest.fixture(scope="module")
def module_fixture():
    print("Setup for module-level fixture")
    yield
    print("Teardown for module-level fixture")

@pytest.fixture(scope="session")
def session_fixture():
    print("Setup for session-level fixture")
    yield
    print("Teardown for session-level fixture")

def test_function_level_fixture(function_fixture):
    print("Running test with function-level fixture")

class TestClass:
    def test_class_level_fixture(self, class_fixture):
        print("Running test with class-level fixture")

def test_module_level_fixture(module_fixture):
    print("Running test with module-level fixture")

def test_session_level_fixture(session_fixture):
    print("Running test with session-level fixture")
```

이 예에서는 범위가 서로 다른 4개의 fixture(`function_fixture`, `class_fixture`, `module_fixture`, `session_fixture`)가 있다. 각 fixture는 리소스의 설정과 해체를 담당한다.

- `function_fixture`의 범위는 `"function"`이다. 이 fixture에 의존하는 각 테스트 함수에 대해 설정과 해체가 수행된다.
- `class_fixture`의 범위는 `"class"`이다. 의존하는 각 테스트 클래스에 대해 한 번씩 설정과 해제된다.
- `module_fixture`의 범위는 `"module"`이다. 각 테스트 모듈에 대해 한 번씩 설정되고 해제된다.
- `session-fixture`의 범위는 `"session"`이다. 전체 테스트 세션에 대해 한 번만 설정되고 해제됨다.

이 에는 각각 해당 fixture를 사용하는 테스트 함수와 테스트 클래스가 포함되어 있다. `pytest`를 실행하면 출력에 각 fixture에 대한 설정, 테스트 실행 및 해체가 표시된다.

```
====================================================== test session starts =======================================================
platform darwin -- Python 3.11.4, pytest-7.3.0, pluggy-1.0.0
rootdir: /Users/yoonjoonlee/MyProjects/PyCodingEx/PyTestExamples
collected 4 items                                                                                                                

ex_04.py ....                                                                                                              [100%]

======================================================= 4 passed in 0.01s ========================================================
```

이 예에서는 각 fixture가 지정된 범위에 따라 설정되고 해체된다. 출력은 테스트 함수와 테스트 클래스와 관련된 각 fixture의 수명 주기를 보여준다.

## fixture 공유

- 여러 테스트 파일에 걸쳐 공유할 fixture를 `conftest.py`에 작성한다.
- 최상위 테스트 디렉터리의 하위 디렉터리에 더 많은 conftest.py 파일을 가질 수 있다. 이렇게 하면 하위 수준의 conftest.py 파일에 정의된 fixture를 해당 디렉터리와 하위 디렉터리의 테스트에서 사용할 수 있다.
- 아무데서나 conftest를 가져오지 마시요. conftest.py 파일은 pytest에서 읽으며 로컬 플러그인으로 간주된다.
- 함수 - `pytest_configure`, 이 안에 있는 변수는 해당 모듈의 모든 테스트 함수에서 사용할 수 있다.

## fixture 추적
- fixture 실행을 추적하려면 `pytest - setup-show` 옵션을 사용한다.

## 호출 테스트 함수를 살펴본다
- 특수 fixture "request"를 사용한다.
- fixture 매개변수의 "request"는 fixture의 호출 상태를 나타내는 또 다른 기본 제공 fixture이다.
- 호출 테스트 함수를 살펴보고 이를 기반으로 결정을 내리는 데 유용하다.

다음은 `pytest`에서 호출 테스트 함수를 들여다보는 방법을 보여주는 예이다.

```python
import pytest

@pytest.fixture(autouse=True)
def introspect_test_function(request):
    test_function = request.node.name
    print(f"Running test function: {test_function}")

def test_example():
    assert 1 + 1 == 2

def test_another_example():
    assert 2 * 3 == 6
```

이 예에서는 `autouse=True` 매개 변수가 있는 `introspect_test_function`이라는 fixture가 있다. 즉, 이 fixture는 매개변수로 명시적으로 언급하지 않아도 모든 테스트 함수에 자동으로 사용된다.

fixture 내부에서는 `pytest`에서 제공하는 `request` 객체를 액세스한다. `request.node.name` 속성은 현재 실행 중인 테스트 함수의 이름을 제공한다.

그런 다음 테스트 함수의 이름을 인쇄하여 현재 실행 중인 테스트를 나타낸다.

`pytest`를 실행하면 출력에 테스트 함수 이름이 표시된다.

```
==================================================================== test session starts ====================================================================
platform darwin -- Python 3.11.4, pytest-7.3.0, pluggy-1.0.0
rootdir: /Users/yoonjoonlee/MyProjects/PyCodingEx/PyTestExamples
collected 2 items

ex_05.py Running test function: test_example
.Running test function: test_another_example
.

===================================================================== 2 passed in 0.01s =====================================================================
```

이 예에서는 `introspect_test_function` fixture가 `test_example`과 `test_another_example` 테스트 함수 모두에 자동으로 사용된다. 호출하는 테스트 함수를 인트로스펙트하고 테스트 실행 전에 그 이름을 출력한다.

## Factories as Fixtures
- "factory as fixture" 패턴은 단일 테스트에서 fixture의 결과가 여러 번 필요한 상황에서 도움이 될 수 있다.
- fixture는 데이터를 직접 반환하는 대신 데이터를 생성하는 함수를 반환한다. 그런 다음 이 함수를 테스트에서 여러 번 호출할 수 있다.
- 괄호 없이 function_name을 반환한다. 함수에 대한 참조가 호출 코드에 전송된다.

다음은 `pytest`에서 factory as a fixture로 사용하는 예이다.

```python
import pytest

class Calculator:
    def __init__(self, initial_value=0):
        self.value = initial_value

    def add(self, num):
        self.value += num

    def subtract(self, num):
        self.value -= num

@pytest.fixture
def calculator_factory():
    calculator = Calculator(initial_value=10)
    yield calculator
    # Cleanup or teardown code can be added here if needed

def test_calculator_add(calculator_factory):
    calculator_factory.add(5)
    assert calculator_factory.value == 15

def test_calculator_subtract(calculator_factory):
    calculator_factory.subtract(3)
    assert calculator_factory.value == 7
```

이 예에는 간단한 계산기를 나타내는 `Calculator` 클래스가 있다. 초기값이 10인 계산기 클래스의 인스턴스를 생성하여 테스트 함수의 fixture로 사용하려고 한다.

`calculator_factory` fixture는 `@pytest.fixture` 데코레이터를 사용하여 정의한다. fixture 내부에서 초기값이 10인 `Calculator` 클래스의 인스턴스를 생성하고 테스트 함수에 반환한다. `yield` 문은 설정 코드와 해체 코드를 구분하는 역할을 한다.

`test_calculator_add` 함수에서는 `calculator_factory` fixture를 매개변수로 사용한다. 테스트 함수 내에서 계산기 객체를 액세스하고 수정할 수 있다. 이 경우 `add` 메서드를 호출하고 값이 올바르게 업데이트되었는지 확인한다.

마찬가지로 `test_calculator_subtract` 함수에서는 `calculator_factory` fixture를 사용하고 `subtract` 메서드를 호출한 후 업데이트된 값을 assert한다.

`pytest`를 실행하면 출력에 테스트 함수의 결과가 표시된다.

```
==================================================================== test session starts ====================================================================
platform darwin -- Python 3.11.4, pytest-7.3.0, pluggy-1.0.0
rootdir: /Users/yoonjoonlee/MyProjects/PyCodingEx/PyTestExamples
collected 2 items

ex_06.py ..

===================================================================== 2 passed in 0.01s =====================================================================
```

이 예에서 두 테스트 함수 모두 `calculator_factory` fixture를 사용하여 `Calculator` 객체를 생성하고 조작한다. 초기 값 10은 테스트 논리에 따라 수정되며, assertion은 연산의 정확성을 검증한다.
